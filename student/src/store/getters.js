const getters = {
  userName: state => state.app.userName,
  rem: state => state.getters.rem,
  loginInfo: state => state.app.loginInfo,
  userToken: state => state.app.userToken,
  uid: state => state.app.uid
}
export default getters
