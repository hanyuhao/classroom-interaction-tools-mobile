import { getItem, setItem } from '@/utils/storge'

const USER_KEY = 'user-Token'
const UID = 'uid'
const state = {
  userName: '',
  rem: false,
  loginInfo: {
    uid: '',
    password: ''
  },
  userToken: getItem(USER_KEY),
  uid: getItem(UID)
}
const mutations = {
  SET_USER_NAME(state, name) {
    state.userName = name
  },
  SET_REM(state, rem) {
    if (rem === true) {
      state.rem = true
    } else {
      state.rem = false
    }
  },
  SET_LOGININFO(state, loginInfo) {
    state.loginInfo = loginInfo
  },
  SET_USERTOKEN(state, data) {
    state.userToken = data
    //为了防止页面刷新丢失 ，我们还需要把数据放到本地存储中，这里是为了持久化数据
    setItem(USER_KEY, state.userToken)
  },
  SET_UID(state, data) {
    state.uid = data
    setItem(UID, state.uid)
  }
}
const actions = {
  // 设置name
  setUserName({ commit }, name) {
    commit('SET_USER_NAME', name)
  }
}
export default {
  state,
  mutations,
  actions
}
