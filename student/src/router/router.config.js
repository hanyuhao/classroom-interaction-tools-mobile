/**
 * 基础路由
 * @type { *[] }
 */
export const constantRouterMap = [
  {
    path: '/login',
    name: 'login',
    component: () => import('@/views/login'),
    meta: {
      title: '登录',
      keepAlive: false
    }
  },
  {
    path: '/',
    redirect: '/home',
    name: 'AppLayout',
    component: () => import('@/views/layouts/index'),
    meta: {
      title: '首页',
      keepAlive: false
    },
    children: [
      {
        path: '/home',
        name: 'Home',
        component: () => import('@/views/home/index'),
        meta: { title: '首页', keepAlive: true, icon: 'home-o' }
      },
      {
        path: '/msg',
        name: 'Msg',
        component: () => import('@/views/msg/index'),
        meta: { title: '消息', keepAlive: false, icon: 'chat-o' }
      },
      {
        path: '/about',
        name: 'About',
        component: () => import('@/views/home/about'),
        meta: { title: '我的', keepAlive: false, icon: 'user-o' }
      }
    ]
  }
]
