import api from './index'
// axios
import request from '@/utils/request'


export default {
  login,
  getUserInfo,
  getUserName,
  getStudentInfo,
  StuLogout
}

// 登录
export function login(data) {
  return request({
    url: api.Login,
    method: 'post',
    data
  })
}

// 用户信息 post 方法
export function getUserInfo(data) {
  return request({
    url: api.UserInfo,
    method: 'post',
    data,
    hideloading: true
  })
}

// 用户名称 get 方法
export function getUserName(params) {
  return request({
    url: api.UserName,
    method: 'get',
    params,
    hideloading: true
  })
}

export function getStudentInfo(uid) {
  return request({
    url: api.studentInfo + '?uid=' + uid + '&role=' + 'student',
    method: 'GET',
    hideloading: true
  })
}

export function StuLogout() {
  return request({
    url: api.stulogout,
    method: 'POST'
  })
}