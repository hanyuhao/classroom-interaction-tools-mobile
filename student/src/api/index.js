const api = {
  Login: '/user/login',
  UserInfo: '/user/userinfo',
  UserName: '/user/name',
  SignByUid: '/sign/data/find',
  userSign: '/sign/data/do',
  studentInfo: '/user',
  stulogout: '/user/logout',
  haveNotcie: '/notice',
  selecteCourse: '/course'
}

export default api
