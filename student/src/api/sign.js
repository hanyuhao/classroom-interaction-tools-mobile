import api from './index'
import request from '@/utils/request'

export default {
  findSignByUid,
  userFinshSign
}

export function findSignByUid(data) {
  return request({
    url: api.SignByUid + '?uid=' + data.uid + '&number=' + data.number + '&size=' + '20',
    method: 'GET'
  })
}

export function userFinshSign(data) {
  return request({
    url: api.userSign,
    method: 'POST',
    data
  })
}
