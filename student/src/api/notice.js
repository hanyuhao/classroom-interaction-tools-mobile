import request from '@/utils/request'
import api from './index'
export default {
    getNoticeList, getCourseById
}
export function getNoticeList() {
    return request({
        url: api.haveNotcie + '?notice.isSystem=' + 'false' + '&bindCourse=' + 'true',
        method: 'GET'
    })
}


export function getCourseById(data) {
    return request({
        url: api.selecteCourse + '?cid=' + data,
        method: 'GET'
    })
}