// 本地环境配置
module.exports = {
  title: '课堂互动工具',
  baseUrl: 'http://localhost:9018', // 项目地址
  baseApi: 'http://jp-tyo-dvm-2.sakurafrp.com:47469', // 本地api请求地址,注意：如果你使用了代理，请设置成'/'
  //为企业微信对接预留接口
  APPID: 'xxx',
  APPSECRET: 'xxx',
  $cdn: 'https://www.sunniejs.cn/static'

}
