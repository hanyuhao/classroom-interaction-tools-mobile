export const getItem = (name) => {
  const data = window.localStorage.getItem(name)
  try {
    return JSON.parse(data)
  } catch {
    return data
  }
}

export const setItem = (name, value) => {
  if (typeof value === 'object') {
    // 如果value 是对象  因为本地存储不可以直接存对象  ，要先把对象转化为字符串
    value = JSON.stringify(value)
  }
  window.localStorage.setItem(name, value)
}

export const removeItem = name => {
  window.localStorage.removeItem(name)
}
