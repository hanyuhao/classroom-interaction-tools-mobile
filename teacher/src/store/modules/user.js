import { login, logout, getInfo } from '@/api/user'
import { getToken, setToken, removeToken } from '@/utils/auth'
import { resetRouter } from '@/router'

const getDefaultState = () => {
  return {
    uid: '',
    token: getToken(),
    name: '',
    avatar: '',
    uid: ''
  }
}

const state = getDefaultState()

const mutations = {
  RESET_STATE: (state) => {
    Object.assign(state, getDefaultState())
  },
  SET_TOKEN: (state, token) => {
    state.token = token
  },
  SET_NAME: (state, name) => {
    state.name = name
  },
  SET_AVATAR: (state, avatar) => {
    state.avatar = avatar
  },
  SET_UID: (state, uid) => {
    state.uid = uid
  }
}

const actions = {
  // user login
  login({ commit }, userInfo) {
    const { uid, password } = userInfo
    return new Promise((resolve, reject) => {
<<<<<<< HEAD
      login({ uid: uid.trim(), password: password }).then(response => {
=======
      login({ uid: uid, password: password }).then(response => {
>>>>>>> 260582f4fde901cf3a21f5fa80db9e662cf4edf4
        const { data } = response
        console.log(data)
        commit('SET_TOKEN', data.token)
        // commit('SET_UID', data.uid)
        setToken(data.token)
        resolve()
      }).catch(error => {
        console.log('err', error)
        reject(error)
      })
    })
  },

  // get user info
  getInfo({ commit, state }) {
    return new Promise((resolve, reject) => {
      getInfo(state.token).then(response => {
        const { data } = response

        if (!data) {
          return reject('Verification failed, please Login again.')
        }

        const { name, avatar, uid } = data
<<<<<<< HEAD
        commit('SET_UID', uid)
=======

>>>>>>> 260582f4fde901cf3a21f5fa80db9e662cf4edf4
        commit('SET_NAME', name)
        commit('SET_AVATAR', avatar)
        commit('SET_UID', uid)
        resolve(data)
      }).catch(error => {
        reject(error)
      })
    })
  },

  // user logout
  logout({ commit, state }) {
    return new Promise((resolve, reject) => {
      logout(state.token).then(() => {
        removeToken() // must remove  token  first
        resetRouter()
        commit('RESET_STATE')
        resolve()
      }).catch(error => {
        reject(error)
      })
    })
  },

  // remove token
  resetToken({ commit }) {
    return new Promise(resolve => {
      removeToken() // must remove  token  first
      commit('RESET_STATE')
      resolve()
    })
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}

