import Vue from 'vue'
import App from './App.vue'
import router from './router.js'
import './plugins/element.js'
//导入全局样式表
import './assets/css/global.css'

import axios from 'axios'
import '../node_modules/ant-design-vue/dist/antd.css';

import Antd from 'ant-design-vue/es'



let instance = axios.create({
  baseURL: 'http://localhost:8068/',
  timeout: 9000

})
//拦截其
instance.interceptors.request.use(config => {
  // console.log(config)
  config.headers.Authentication = window.sessionStorage.getItem('token')
  //在最后必须returnconfig
  return config
})


//axios.defaults.baseURL = 'http://localhost:8068'
// axios.defaults.withCredentials = true;

Vue.use(Antd)
Vue.prototype.$http = instance


Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
