import Vue from 'vue'
import VueRouter from 'vue-router'
import Login from './components/Login.vue'
import Home from './components/Home.vue'
import Shouye from './components/Shouye.vue'
import StuManger from './components/Manger/StuManger.vue'

const originalPush = VueRouter.prototype.push
VueRouter.prototype.push = function push(location) {
  return originalPush.call(this, location).catch(err => err)
}

Vue.use(VueRouter)

const routes = [
  { path: '/', redirect: '/login' },
  { path: '/login', component: Login },
  {
    path: '/home',
    component: Home,
    redirect: '/111',
    children: [
      { path: '/111', component: Shouye },
      { path: '/stuManger', component: StuManger }
    ]
  }
]

const router = new VueRouter({
  routes
})
// 挂载路由导航卫士
router.beforeEach((to, from, next) => {
  //如果访问login  直接放行
  if (to.path === '/login') return next()
  //获取token
  const tokenStr = window.sessionStorage.getItem('token')

  if (!tokenStr) return next('/login')
  next()
}


)

export default router
